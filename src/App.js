import { useState, useEffect } from "react";
import "./App.css";

function App() {
  useEffect(()=>
  {
    console.log("Hello World");

  },[]);
  const[counter,setCounter]=useState(0);
  const increaseCounter=()=>
  {
    setCounter(counter+1);
  }
  return (
    <div className="App"> 
     The below button has been clicked {counter} times.
      <button className="app-button" onClick={increaseCounter}>Increase counter</button>
    </div>
  );
}

export default App;
